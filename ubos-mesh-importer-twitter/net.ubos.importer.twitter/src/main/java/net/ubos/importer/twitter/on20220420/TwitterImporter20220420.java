//
// Copyright (C) Johannes Ernst. All rights reserved. License: see package.
//

package net.ubos.importer.twitter.on20220420;

import net.ubos.importer.fallback.AbstractFallbackImporter;

/**
 * Twitter importer
 */
public class TwitterImporter20220420
    extends
        AbstractFallbackImporter
{

}
